# python utility to scrape twitter for mentions to a user
# the tweets are output to a json file for use elsewhere
# run as a cron job for regular updates

# import system libraries
import os, json, sys, getopt, configparser, ast

# requires python-twitter
import twitter

# main function
if __name__ == "__main__":
    # fetch info from the config file
    try:
        opts, args = getopt.getopt(sys.argv[1:], "", ["config="])

    except (Exception, getopt.GetoptError):
        print("Specify a config file with --config=<file name>")
        sys.exit(1)

    if len(opts) < 1:
        print("Specify a file with --config=<file name>")
        sys.exit(1)

    config_file = None

    for o, a in opts:
        if o == "--config":
            config_file = a

    config = configparser.ConfigParser()
    config.read(config_file)

    twitter_api = twitter.Api(
        config['TWITTER']['consumer_key'],
        config['TWITTER']['consumer_secret'],
        config['TWITTER']['access_token_key'],
        config['TWITTER']['access_token_secret'])

    # get tweets to @wmtu_requests
    tweets = twitter_api.GetMentions(count=10, return_json=True)

    # output tweets to file
    with open(config['GENERAL']['json_file'], 'w') as out_file:
        json.dump(tweets, out_file)

    sys.exit(0)
