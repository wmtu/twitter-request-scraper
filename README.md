# Twitter Request Scraper

Grabs specified tweets from the twitter API and dumps the result to a JSON file for easy access.

## Requirements

- python 3
- python-twitter
